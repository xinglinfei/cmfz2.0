package com.fourthtimer.cmfz;

import org.apache.catalina.security.SecurityUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

import javax.xml.transform.Source;
import java.sql.SQLOutput;

public class ShiroTest {
    /**
     * 假数据 模拟用户浏览器输入的帐号密码
     */
    private String username="zhangsan";
    private String password="123456";
    @Test
    public void test1(){
        /**
         * 1、读取配置文件 就相当于获取了数据库的数据
         *
         *      1、获取安全管理器工厂
         *      2、获取安全管理器SerurityManager
         */
        IniSecurityManagerFactory iniSecurityManagerFactory = new IniSecurityManagerFactory("classpath:shiro.ini");
        SecurityManager securityManager = iniSecurityManagerFactory.getInstance();
        /**
         * 2、将安全管理器和Subject建立联系
         */
        SecurityUtils.setSecurityManager(securityManager);
        /**
         * 3、获取subject
         */
        Subject subject = SecurityUtils.getSubject();
        /**
         * 4、把用户输入的帐号密码封装到令牌中
         * token 令牌  封装身份信息的一个中间对象
         *
         */
        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        /**
         * 5、调用login方法的同时，将令牌给subject
         *
         * 我们只需要给数据 账号密码的校验 框架自己来做
         *
         * 通过抛异常的方式 告诉程序员有没有认证成功
         * 没有异常就是成功了
         *
         * UnknownAccountException 帐号不存在异常
         * IncorrectCredentialsException 密码不正确
         */
        try {
            subject.login(token);
            System.out.println("认证成功");
        } catch (Exception e) {
//            e.printStackTrace();
            System.out.println("认证失败");
        }
        /**
         * Authenticated 认证
         * 在shiro中 authen开头的任何词语都和认证有关系
         *
         * isAuthenticated()查看当前主体的认证状态
         */
        boolean authenticated = subject.isAuthenticated();
        System.out.println(authenticated);
    }
    @Test
    public void test2(){
        //1、读取配置文件，获取安全管理器
        SecurityManager securityManager = new IniSecurityManagerFactory("classpath:shiro.ini").getInstance();
        //2、将安全管理类给工具类
        SecurityUtils.setSecurityManager(securityManager);
        //3、获取主体
        Subject subject = SecurityUtils.getSubject();
        //4、用户名和密码封装token
        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        //5、调用login方法
        try {
            subject.login(token);
            System.out.println("成功");
        } catch (AuthenticationException e) {
            System.out.println("登录失败");
        }

        if(subject.isAuthenticated()){
            //认证通过之后，可以进行授权操作 代码式授权，通过主体可以校验角色和权限
            /**
             * hasRole 校验当前用户有没有某个角色
             */
            boolean vip1 = subject.hasRole("vip1");
            if(vip1){
                System.out.println("调用上师方法");
            }else{
                System.out.println("可以自定义抛异常 或者可以直接return null");
            }
            /**
             * isPermitted校验当前用户有没有某个权限
             */
            boolean permitted = subject.isPermitted("guru:select");
            if(permitted){
                System.out.println("可以调用查询上师的方法");
            }else{
                System.out.println("没有查询上师的权限");
                throw new RuntimeException("没有权限");
            }
        }
    }
}
