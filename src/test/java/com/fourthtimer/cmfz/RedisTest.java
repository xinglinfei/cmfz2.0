package com.fourthtimer.cmfz;

import com.fourthtimer.cmfz.service.MenuService;
import org.aspectj.lang.annotation.Aspect;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.HashMap;
import java.util.Map;

/*
 * redisTemplate 操作redis的类
 */
public class RedisTest extends CmfzApplicationTests{
    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    public void test1(){
        /**
         * opsForValue 创建一个操作String类型的对象
         * opsForHash 创建一个操作hash类型的对象
         *
         * 操作redis的方法和命令以及jedis的方法类似
         */
        ValueOperations stringoPs = redisTemplate.opsForValue();
//        stringoPs.set("测试","redis");
        Object o = stringoPs.get("测试");
        System.out.println(o);
    }

    /**
     * opsForHash
     */
    @Test
    public void test2(){
        HashOperations hashOperations = redisTemplate.opsForHash();
        Map map = new HashMap();
        map.put("zhangsan","123456");
        map.put("lisi","123456");
        hashOperations.putAll("admins",map);
        Object admins = hashOperations.get("admins","zhangsan");
        System.out.println(admins);
//        删除key
//        redisTemplate.delete("admins");
    }
    @Autowired
    private MenuService menuService;
    @Test
    public void test3(){
        menuService.selectAll();
    }
}
