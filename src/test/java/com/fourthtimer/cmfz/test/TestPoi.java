package com.fourthtimer.cmfz.test;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.fourthtimer.cmfz.CmfzApplicationTests;
import com.fourthtimer.cmfz.dao.GuruDao;
import com.fourthtimer.cmfz.entity.Guru;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileOutputStream;
import java.util.List;

public class TestPoi extends CmfzApplicationTests {
    @Autowired(required = true)
    private GuruDao guruDao;
    @org.junit.Test
    public void testPoi() throws Exception {
        //所有的上师数据
        List<Guru> guruList = guruDao.selectAll(0, 100, null);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("所有上市数据", "guru"),Guru.class,guruList);
        workbook.write(new FileOutputStream("D://easyTest.xls"));
    }
}
