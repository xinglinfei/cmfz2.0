package com.fourthtimer.cmfz.test;

import com.fourthtimer.cmfz.CmfzApplicationTests;
import com.fourthtimer.cmfz.dao.LogDao;
import com.fourthtimer.cmfz.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;


public class LogTest extends CmfzApplicationTests {
    @Autowired
    private LogDao logDao;
    @Autowired
    private LogService logService;
    @org.junit.Test
    public void log(){
//        System.out.println(logDao.selectByPage(0,10,null));
        System.out.println(logService.selectByPage(1,10,null));
    }
}
