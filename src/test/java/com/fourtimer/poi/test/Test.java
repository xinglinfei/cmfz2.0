package com.fourtimer.poi.test;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Test {
    /**
     * 生成Excel
     */
    @org.junit.Test
    public void createExcel() throws IOException {
        //1、新建一个Excel文件 HSSFWorkbook
        HSSFWorkbook workbook = new HSSFWorkbook();
        //2、在Excel文件中新建一张工作表 HSSFSheet
        HSSFSheet guru = workbook.createSheet("guru");
        //3、在工作表中选中哪一行写数据 第四行 HSSFRow 下标从0开始
        HSSFRow row = guru.createRow(3);
        //4、在这一行中选中在哪一个单元格中写数据 第三个 HSSGFCell 下标从0开始
        HSSFCell cell = row.createCell(2);
        //5、在单元格中写数据 HSSFCell.setCellValue()
        cell.setCellValue("哈哈怪");
        //6、保存本地 xls
        workbook.write(new FileOutputStream("D:\\Programming\\预热资料\\终期项目\\3 poi - day3\\guru.xls"));
    }


    /**
     * excel文件导入
     *
     * 文件读取内存中 通过poi解析流
     */
    @org.junit.Test
    public void exportExcel() throws IOException {
        //1、读取文件
        FileInputStream fileInputStream = new FileInputStream("D:\\Programming\\预热资料\\终期项目\\3 poi - day3\\guru.xls");
        //2、通过poi解析流
        HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
        //3、通过文件对象拿到表对象 通过下标
        HSSFSheet sheet = workbook.getSheetAt(0);
        //4、通过表对象拿到行对象
        HSSFRow row = sheet.getRow(3);
        //5、通过行对象拿到单元格对象
        HSSFCell cell = row.getCell(2);
        //6、从单元格中拿到数据
        String value = cell.getStringCellValue();
        System.out.println(value);
    }
}
