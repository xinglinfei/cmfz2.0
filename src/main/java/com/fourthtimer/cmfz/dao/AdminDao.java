package com.fourthtimer.cmfz.dao;

import com.fourthtimer.cmfz.entity.Admin;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminDao {
    Admin selectAdminByUsernameAndPassword(String username,String password);

    List<Admin> selectByPage(@Param("offset") int offset,@Param("limit") int limit);

    int selectAdminCount();

    void insert(Admin admin);

    void delete(int[] ids);

    void update(Admin admin);

    Admin selectOneAdmin(int id);

    Admin selectOneAdminByUsername(String username);
}
