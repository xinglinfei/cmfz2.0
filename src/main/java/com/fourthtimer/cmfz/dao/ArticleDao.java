package com.fourthtimer.cmfz.dao;

import com.fourthtimer.cmfz.entity.Article;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleDao {

    List<Article> selectByPage(@Param("offset") int offset,@Param("limit") int limit);
    int articleCount();

    void insert(Article article);

    void delete(int[] articleIds);

    void update(Article article);

    Article selectOneArticle(int articleId);

}
