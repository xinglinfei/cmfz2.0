package com.fourthtimer.cmfz.dao;

import com.fourthtimer.cmfz.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserDao{

    List<User> selectByPage(@Param("offset") int offset, @Param("limit") int limit);
    int selectUserCount();

    void insert(User user);

    void delete(int[] userIds);

    void update(User user);

    User selectOneUser(int userId);

    void miltiInsert(List<User> userList);

    List<Map> selectBySexCount();

    List<Map> selectByProvince();

    /**
     * mybatis接口传入参数
     * 1、使用对象
     * 2、使用@param本质上也是把参数封装到Map中
     * 3、使用Map mapper中通过key来取值
     *
     * 在传参和收参中可以直接使用map等价于对象
     * @param max
     * @param min
     */

    int selectByDayCount(@Param("max") Integer max,@Param("min") Integer min);


}
