package com.fourthtimer.cmfz.dao;

import com.fourthtimer.cmfz.entity.Banner;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BannerDao {
    //查询
    List<Banner> selectByPage(@Param("offset") int offset,@Param("limit") int limit);

    int selectCount();
    //增加s
    void insert(Banner banner);
    //删除
    void delete(int[] bannerIds);
    //单查
    Banner selectOneBanner(int bannerId);
    //修改
    void update(Banner banner);
}
