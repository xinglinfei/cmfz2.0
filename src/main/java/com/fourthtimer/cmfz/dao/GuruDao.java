package com.fourthtimer.cmfz.dao;

import com.fourthtimer.cmfz.entity.Guru;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GuruDao {
    List<Guru> selectAll(@Param("offset")int offset,@Param("limit")int limit,@Param("name")String guruNickName);
    int selectCount(@Param("name")String guruNickName);

    void insert(Guru guru);

    void delete(int[] guruIds);

    //单查
    Guru selectOneGuru(int guruId);
    //修改
    void update(Guru guru);
    //恢复
    void recover(int guruId);


    /**
     * 批量导入
     *
     */
    void multiupload();

}
