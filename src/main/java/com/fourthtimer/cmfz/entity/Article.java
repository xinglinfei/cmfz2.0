package com.fourthtimer.cmfz.entity;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Article {
    private Integer articleId;
    private String articleName;
    private String articleImage;
    private String articleContent;
    private Date articleDate;
    private Guru guru;
}
