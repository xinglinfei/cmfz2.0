package com.fourthtimer.cmfz.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Resource {
    @JsonProperty(value = "id")
    private Integer resourceId;
    @JsonProperty(value = "title")
    private String resourceName;
    private String resourceUrl;
    private String resourceType;
    private String resourcePermission;
    private String resourceParentId;
    @JsonProperty(value = "children")
    private List<Resource> childrens;
    /**
     * 如果使用mybatis-plus时
     * 如果有属性不是数据库的字段，但在项目中必须使用
     * 这样在新增等使用bean的时候，，mybatis-plus就会忽略这个，不会报错
     */
}
