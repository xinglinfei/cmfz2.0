package com.fourthtimer.cmfz.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Role {
    private Integer roleId;
    private String roleName;
}
