package com.fourthtimer.cmfz.entity;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class Log {
    private Integer logId;
    private String logAdminName;
    private Date logDate;
    private String logIp;
    private String logType;
    private String logContent;

}
