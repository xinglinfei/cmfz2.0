package com.fourthtimer.cmfz.entity;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class Menu implements Serializable {
    private Integer menuId;
    private String menuName;
    private String menuUrl;
    private String menuParentId;
    private List<Menu> child;
}
