package com.fourthtimer.cmfz.entity;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class Banner {
    private Integer bannerId;
    private String bannerImageUrl;
    private String bannerOldName;
    private Integer bannerState;
    private Date bannerDate;
    private String bannerDescription;
}
