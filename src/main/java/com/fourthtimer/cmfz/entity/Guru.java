package com.fourthtimer.cmfz.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.*;

import java.io.Serializable;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class Guru implements Serializable {
    @Excel(name = "上师编号")
    private Integer guruId;
    @Excel(name = "上师名字")
    private String guruName;
    @Excel(name = "上师图片地址")
    private String guruImage;
    @Excel(name = "上师法号")
    private String guruNickName;
    @Excel(name = "上师状态")
    private Integer guruStatus;

}

