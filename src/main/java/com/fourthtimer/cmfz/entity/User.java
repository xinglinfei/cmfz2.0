package com.fourthtimer.cmfz.entity;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class User {
    private Integer userId;
    private String userTelephone;
    private String userPassword;
    private String userImage;
    private String userNickName;
    private String userName;
    private String userSex;
    private String userAutograph;
    private String userProvince;
    private String userCity;
    private Integer userStatus;
    private Date userCreateDate;
    private Guru guru;
}
