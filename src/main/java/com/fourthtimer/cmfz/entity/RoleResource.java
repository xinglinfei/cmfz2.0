package com.fourthtimer.cmfz.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class RoleResource {
    private Integer id;
    private String roleName;
    private Integer resourceId;
}
