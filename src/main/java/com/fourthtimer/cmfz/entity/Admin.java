package com.fourthtimer.cmfz.entity;


import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
//JDK序列化  在redis中能正常显示
public class Admin implements Serializable {
    private Integer id;
    private String username;
    private String password;
}
