package com.fourthtimer.cmfz.entity;

import lombok.Data;

@Data
public class AdminRole {
    private Integer id;
    private Integer adminId;
    private Integer roleId;
}
