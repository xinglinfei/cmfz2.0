package com.fourthtimer.cmfz.service;

import com.fourthtimer.cmfz.entity.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    Map selectAll(int page,int limit);

    Map insert(User user);

    Map delete(int[] userIds);

    Map update(User user);

    User selectOneUser(int userId);

    List<Map> selectBySexCount();

    List<Map> selectByProvince();

    Map selectByDayCount();

    Map selectByThread() throws InterruptedException;
}
