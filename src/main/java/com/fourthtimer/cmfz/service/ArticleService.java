package com.fourthtimer.cmfz.service;

import com.fourthtimer.cmfz.entity.Article;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

public interface ArticleService {

    Map selectAll(int page,int limit);

    Map insert(Article article);

    Map delete(int[] articleIds);

    Map update(Article article);

    Article selectOneArticle(int articleId);

}
