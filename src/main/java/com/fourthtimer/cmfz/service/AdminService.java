package com.fourthtimer.cmfz.service;


import com.fourthtimer.cmfz.entity.Admin;

import java.util.Map;

public interface AdminService {
    Boolean login(String username, String password);

    Map selectByPage(int page, int limit);

    Map insert(Admin admin);

    Map delete(int[] ids);

    Map update(Admin admin);

    Admin selectOneAdmin(int id);
}
