package com.fourthtimer.cmfz.service;

import com.fourthtimer.cmfz.entity.Banner;

import java.util.List;
import java.util.Map;

public interface BannerService {
    //查询
    Map selectAll(int page,int limit);
    //增加
    Map insert(Banner banner);
    //删除
    Map delete(int[] bannerIds);
    //单查
    Banner selectOneBanner(int bannerId);
    //修改
    Map update(Banner banner);
}
