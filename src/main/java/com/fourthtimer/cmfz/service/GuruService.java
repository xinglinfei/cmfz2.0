package com.fourthtimer.cmfz.service;

import com.fourthtimer.cmfz.entity.Guru;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

public interface GuruService {
    Map selectByPage(int page,int limit,String name);

    Map insert(Guru guru);

    Map delete(int[] guruIds);

    Guru selectOneGuru(int guruId);

    Map update(Guru guru);

    Map recover(int guruId);

    //批量导出
    void multiDownload(HttpServletResponse response) throws Exception;

    void multiUpload(MultipartFile file) throws Exception;
}
