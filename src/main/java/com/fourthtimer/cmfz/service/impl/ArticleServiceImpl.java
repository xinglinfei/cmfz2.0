package com.fourthtimer.cmfz.service.impl;

import com.fourthtimer.cmfz.dao.ArticleDao;
import com.fourthtimer.cmfz.entity.Article;
import com.fourthtimer.cmfz.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleDao articleDao;
    @Override
    public Map selectAll(int page, int limit) {
        int offset = (page-1)*limit;
        System.out.println("offset"+offset);
        List<Article> articleList = articleDao.selectByPage(offset, limit);
        int articleCount = articleDao.articleCount();
        Map map = new HashMap();
        map.put("code","");
        map.put("msg","");
        map.put("count",articleCount);
        map.put("data",articleList);
        return map;
    }


    @Override
    public Map insert(Article article) {
        Map map = new HashMap();
        article.setArticleDate(new Date());
        try {
            articleDao.insert(article);
            map.put("isInsert",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isInsert",false);
        }
        return map;
    }

    @Override
    public Map delete(int[] articleIds) {
        Map map = new HashMap();
        try {
            articleDao.delete(articleIds);
            map.put("isDelete",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isDelete",false);
        }
        return map;
    }

    @Override
    public Map update(Article article) {
        Map map = new HashMap();
        article.setArticleDate(new Date());
        try {
            articleDao.update(article);
            map.put("isUpdate",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isUpdate",false);
        }
        return map;
    }

    @Override
    public Article selectOneArticle(int articleId) {
        return articleDao.selectOneArticle(articleId);
    }
}
