package com.fourthtimer.cmfz.service.impl;

import com.fourthtimer.cmfz.dao.GuruDao;
import com.fourthtimer.cmfz.entity.Guru;
import com.fourthtimer.cmfz.service.GuruService;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GuruServiceImpl implements GuruService {
    @Autowired
    private GuruDao guruDao;

    @Override
    public Map selectByPage(int page, int limit, String name) {
        if (name == null || name.equals("")) {
            name = null;
        } else {
            name = "%" + name + "%";
        }
        int offset = (page - 1) * limit;
        System.out.println("name=" + name + "\n" + "offset=" + offset);
        List<Guru> guruList = guruDao.selectAll(offset, limit, name);
        int count = guruDao.selectCount(name);
        Map map = new HashMap();
        map.put("code", 0);
        map.put("msg", "");
        map.put("count", count);
        map.put("data", guruList);
        return map;
    }

    @Override
    public Map insert(Guru guru) {
        Map map = new HashMap();
        try {
            map.put("isInsert", true);
            guruDao.insert(guru);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isInsert", false);
        }
        return map;
    }

    @Override
    public Map delete(int[] guruIds) {
        System.out.println("dao删除" + guruIds);
        Map map = new HashMap();
        try {
            map.put("isDelete", true);
            guruDao.delete(guruIds);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isDelete", false);
        }
        return map;
    }


    @Override
    public Guru selectOneGuru(int guruId) {
        Guru guru = guruDao.selectOneGuru(guruId);
        return guru;
    }

    @Override
    public Map update(Guru guru) {
        Map map = new HashMap();
        try {
            map.put("isUpdate", true);
            guruDao.update(guru);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isUpdate", false);
        }
        return map;
    }

    @Override
    public Map recover(int guruId) {
        Map map = new HashMap();
        try {
            map.put("isRecover", true);
            guruDao.recover(guruId);
        } catch (Exception e) {
            map.put("isRecover", false);
        }
        return map;
    }

    @Override
    public void multiDownload(HttpServletResponse response) throws Exception {
        //获取到所有的数据
        //这里写死了   以后可以多谢一个查全部方法 或者动态sql
        //0、准备数据
        List<Guru> guruList = guruDao.selectAll(0, 100, null);
        String[] titles = {"上师编号", "上师名字", "上师图片地址", "上师法名", "上师状态"};
        //1、新建一个Excel文件 HSSFWorkbook
        HSSFWorkbook workbook = new HSSFWorkbook();
        //2、在Excel文件中新建一张工作表HSSFShoot
        HSSFSheet gurus = workbook.createSheet("gurus");
        //3、在工作表中写入标题数据
        HSSFRow gurusRow = gurus.createRow(0);
        for (int i = 0; i < titles.length; i++) {
            //创建单元格
            HSSFCell gurusRowCell = gurusRow.createCell(i);
            //在单元格中写入数据
            gurusRowCell.setCellValue(titles[i]);
        }
        //4、在工作表中写入上师的数据
        for (int i = 0; i < guruList.size(); i++) {
            //拿到被便利到的对象
            Guru guru = guruList.get(i);
            //创建行
            HSSFRow row = gurus.createRow(i + 1);
            //向行中写入数据
            //1、得到类对象
            Class<? extends Guru> guruClass = guru.getClass();
            //2、获取所有的属性对象(Declared  包含私有的属性)
            Field[] declaredFields = guruClass.getDeclaredFields();
            //3、遍历数组创建单元格
            for (int j = 0; j < declaredFields.length; j++) {
                //4、获取属性值
                Field declaredField = declaredFields[j];
                //设置为可访问
                declaredField.setAccessible(true);
                String value = declaredField.get(guru).toString();
                //5、创建单元格 赋值
                row.createCell(j).setCellValue(value);
            }
        }
        //5、保存
//        FileOutputStream fileOutputStream = new FileOutputStream("D:/Programming/预热资料/终期项目/3 poi - day3/guru.xls");
        workbook.write(new FileOutputStream("D:/Programming/预热资料/终期项目/3 poi - day3/guru.xls"));


        //下载出来
        response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode("guru.xls", "utf-8"));
        response.setContentType("application/vnd.ms-excel");
        OutputStream outputStream = response.getOutputStream();
        FileUtils.copyFile(new File("D:/Programming/预热资料/终期项目/3 poi - day3/guru.xls"),outputStream);
    }

    @Override
    public void multiUpload(MultipartFile file) throws Exception {
        System.out.println(file+"hhhhhhs");
        InputStream inputStream = file.getInputStream();
        HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
        HSSFSheet sheet = workbook.getSheetAt(0);
        int lastRowNum = sheet.getLastRowNum();
        for(int i=1;i<=lastRowNum;i++){
            //获取行
            HSSFRow row = sheet.getRow(i);
            //获取行中的数据   分装对象
            Guru guru = new Guru();

            int id = (int) row.getCell(0).getNumericCellValue();
            guru.setGuruId(id);
            guru.setGuruName(row.getCell(1).getStringCellValue());
            guru.setGuruImage(row.getCell(2).getStringCellValue());
//            System.out.println(row.getCell(2).getStringCellValue());
//            guru.setGuruImage("EEEE");
            guru.setGuruNickName(row.getCell(3).getStringCellValue());
//            System.out.println(row.getCell(3).getNumericCellValue());
//            guru.setGuruNickName("AAAAA");
            guru.setGuruStatus((int) row.getCell(4).getNumericCellValue());
            System.out.println(guru);
            guruDao.insert(guru);
        }
    }
}
