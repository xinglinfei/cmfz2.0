package com.fourthtimer.cmfz.service.impl;

import cn.hutool.captcha.ICaptcha;
import com.fourthtimer.cmfz.dao.AdminDao;
import com.fourthtimer.cmfz.entity.Admin;
import com.fourthtimer.cmfz.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.Servlet;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminDao adminDao;

    @Override
    public Boolean login(String username, String password) {
        Boolean flag = false;
        Admin admin = adminDao.selectAdminByUsernameAndPassword(username, password);
        if (admin != null && admin.getPassword().equals(password)) {
            return flag = true;
        }
        return flag;
    }

    @Override
    public Map selectByPage(int page, int limit) {
        int offset = (page-1)*limit;
        System.out.println("offset"+offset);
        List<Admin> adminList = adminDao.selectByPage(offset, limit);
        System.out.println(adminList);
        int adminCount = adminDao.selectAdminCount();
        Map map = new HashMap();
        map.put("code","");
        map.put("msg","");
        map.put("count",adminCount);
        map.put("data",adminList);
        return map;
    }


    @Override
    public Map insert(Admin admin) {
        Map map = new HashMap();
        try {
            adminDao.insert(admin);
            map.put("idInsert",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("idInsert",false);
        }
        return map;
    }

    @Override
    public Map delete(int[] ids) {
        Map map = new HashMap();
        try {
            adminDao.delete(ids);
            map.put("idDelete",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("idDelete",false);
        }
        return map;
    }

    @Override
    public Map update(Admin admin) {
        Map map = new HashMap();
        try {
            adminDao.update(admin);
            map.put("idUpdate",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("idUpdate",false);
        }
        return map;
    }

    @Override
    public Admin selectOneAdmin(int id) {
        return adminDao.selectOneAdmin(id);
    }
}
