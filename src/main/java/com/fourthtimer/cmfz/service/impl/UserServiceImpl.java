package com.fourthtimer.cmfz.service.impl;

import com.fourthtimer.cmfz.annotation.AddCacheAnnotation;
import com.fourthtimer.cmfz.dao.UserDao;
import com.fourthtimer.cmfz.entity.Guru;
import com.fourthtimer.cmfz.entity.User;
import com.fourthtimer.cmfz.service.UserService;
import com.fourthtimer.cmfz.util.ThreadPoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserDao userDao;

    @Override
    public Map selectAll(int page, int limit) {
        int offset = (page-1)*limit;
        System.out.println("offset"+offset);
        List<User> userList = userDao.selectByPage(offset, limit);
        int userCount = userDao.selectUserCount();
        Map map = new HashMap();
        map.put("code","");
        map.put("msg","");
        map.put("count",userCount);
        map.put("data",userList);
        return map;
    }

    @Override
    public Map insert(User user) {
        Map map = new HashMap();
        user.setUserCreateDate(new Date());
        user.setGuru(new Guru(1,null,null,null,null));
        try {
            userDao.insert(user);
            map.put("isInsert",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isInsert",false);
        }
        return map;
    }

    @Override
    public Map delete(int[] userIds) {
        Map map = new HashMap();
        try {
            userDao.delete(userIds);
            map.put("isDelete",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isDelete",false);
        }
        return map;
    }

    @Override
    public Map update(User user) {
        Map map = new HashMap();
        user.setUserCreateDate(new Date());
        user.setGuru(new Guru(1,null,null,null,null));
        try {
            userDao.update(user);
            map.put("isUpdate",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isUpdate",false);
        }
        return map;
    }

    @Override
    public User selectOneUser(int userId) {
        return userDao.selectOneUser(userId);
    }

    /**
     * Logger loggerFactory都是org.slf4j包的
     * 参数是当前类的类对象
     */
     private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    @AddCacheAnnotation
    public List<Map> selectBySexCount() {
        return userDao.selectBySexCount();
    }

    @Override
    @AddCacheAnnotation
    public List<Map> selectByProvince() {
        return userDao.selectByProvince();
    }

    @Override
    @AddCacheAnnotation
    public Map selectByDayCount() {
        Map map = new HashMap();
        map.put("one",userDao.selectByDayCount(7,1));
        map.put("two",userDao.selectByDayCount(14,7));
        map.put("three",userDao.selectByDayCount(21,14));
        return map;
    }

    @Override
    @AddCacheAnnotation
    public Map selectByThread() throws InterruptedException {
        final Map map = new HashMap();
        //创建一个计数器对象
        final CountDownLatch countDownLatch = new CountDownLatch(3);
        //1、创建三个线程          匿名内部类
        ThreadPoolUtil.executor.submit(new Runnable() {
            @Override
            public void run() {
                //注册量
                Map countMap = selectByDayCount();
                map.put("count",countMap);
                //计数器减一
                countDownLatch.countDown();
            }
        });
        ThreadPoolUtil.executor.submit(new Runnable() {
            @Override
            public void run() {
                List<Map> sexMap = selectBySexCount();
                map.put("sexMap",sexMap);
                countDownLatch.countDown();
            }
        });
        ThreadPoolUtil.executor.submit(new Runnable() {
            @Override
            public void run() {
                List<Map> provinceMap = selectByProvince();
                map.put("provinceMap",provinceMap);
                countDownLatch.countDown();
            }
        });

        //2、控制主线程的等待子线程执行结束
        countDownLatch.await();

        return map;
    }
}
