package com.fourthtimer.cmfz.service.impl;

import com.fourthtimer.cmfz.annotation.AddCacheAnnotation;
import com.fourthtimer.cmfz.annotation.DeleteCacheAnnotation;
import com.fourthtimer.cmfz.dao.MenuDao;
import com.fourthtimer.cmfz.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuDao menuDao;
    @Override
    @AddCacheAnnotation
    public List selectAll() {
        return menuDao.selectAll();
    }
}
