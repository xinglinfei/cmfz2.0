package com.fourthtimer.cmfz.service.impl;

import com.fourthtimer.cmfz.dao.LogDao;
import com.fourthtimer.cmfz.entity.Log;
import com.fourthtimer.cmfz.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogDao logDao;
    @Override
    public Map selectByPage(int page, int limit, String name) {
        if(name==null||"".equals(name)){
            name=null;
        }else{
            name="%"+name+"%";
        }
        int offset=(page-1)*limit;
        System.out.println(name+"\n"+offset);
        List<Log> logList = logDao.selectByPage(offset, limit, name);
        int logCount = logDao.selectLogCount(name);
        Map map = new HashMap();
        map.put("code",0);
        map.put("msg","");
        map.put("count",logCount);
        map.put("data",logList);
        return map;
    }
}
