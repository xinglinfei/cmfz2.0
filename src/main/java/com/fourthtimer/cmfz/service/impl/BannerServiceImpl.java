package com.fourthtimer.cmfz.service.impl;

import com.fourthtimer.cmfz.dao.BannerDao;
import com.fourthtimer.cmfz.entity.Banner;
import com.fourthtimer.cmfz.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BannerServiceImpl implements BannerService {
    @Autowired
    private BannerDao bannerDao;

    @Override
    public Map selectAll(int page, int limit) {
        Map map = new HashMap();
        int offset = (page-1)*limit;
        System.out.println("offset"+offset);
        List<Banner> bannerList = bannerDao.selectByPage(offset, limit);
        int count = bannerDao.selectCount();
        map.put("code","");
        map.put("msg","");
        map.put("count",count);
        map.put("data",bannerList);
        return map;
    }

    @Override
    public Map insert(Banner banner) {
        Map map = new HashMap();
        banner.setBannerDate(new Date());
        try {
            bannerDao.insert(banner);
            map.put("isInsert",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isInsert",false);
        }
        return map;
    }

    @Override
    public Map delete(int[] bannerIds) {
        Map map = new HashMap();
        try {
            bannerDao.delete(bannerIds);
            map.put("isDelete",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isDelete",false);
        }
        return map;
    }

    @Override
    public Banner selectOneBanner(int bannerId) {
        Banner banner = bannerDao.selectOneBanner(bannerId);
        return banner;
    }

    @Override
    public Map update(Banner banner) {
        Map map = new HashMap();
        banner.setBannerDate(new Date());
        try {
            bannerDao.update(banner);
            map.put("isUpdate",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("isUpdate",false);
        }
        return map;
    }
}
