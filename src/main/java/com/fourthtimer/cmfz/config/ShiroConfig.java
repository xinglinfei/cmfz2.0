package com.fourthtimer.cmfz.config;

import com.fourthtimer.cmfz.realm.AuthenRealm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {
    /**
     * 过滤器工厂
     *
     * @Bean 所在的方法的方法形参 如果形参对应的类型在工厂中存在一个对象
     *
     * 这个对象会被自动装配上
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager defaultWebSecurityManager){
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        /**
         * 配置拦截规则
         * anon 匿名可访问
         * authc 认证通过可访问
         */
        Map map = new HashMap<>();
        map.put("/login.jsp","anon");
        map.put("/manage.jsp","authc");
        map.put("/main.jsp","authc");
        map.put("/admin/*","anon");
        map.put("/menu/*","authc");
        map.put("/user/*","authc");
        map.put("/guru/*","authc");
        map.put("/article/*","authc");
        map.put("/banner/*","authc");
        map.put("/log/*","authc");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(map);
        /**
         * 设置安全管理器
         */
        shiroFilterFactoryBean.setSecurityManager(defaultWebSecurityManager);
        return shiroFilterFactoryBean;
    }
    /**
     * 创建安全管理器
     */
    @Bean
    public DefaultWebSecurityManager defaultWebSecurityManager(AuthenRealm authenRealm){
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        //设置自定义Realm
        defaultWebSecurityManager.setRealm(authenRealm);

        return defaultWebSecurityManager;
    }
    /**
     * 自定义Realm
     */
    @Bean
    public AuthenRealm authenRealm(){
        return new AuthenRealm();
    }
}
