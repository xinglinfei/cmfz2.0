package com.fourthtimer.cmfz.api;

import org.springframework.web.bind.annotation.*;

/**
 * @RestController
 */

@RestController
@RequestMapping("rest")
public class TestRestController {
    /**
     * 让这个方法只能通过get方式请求到
     * rest相关注解
     * @GetMapping
     * @PostMapping
     * @PutMapping
     * @DeleteMapping
     */
//    @RequestMapping(path="testGet/{name}",method = RequestMethod.GET)
    @GetMapping("testGet/{name}")
    public String testGet(@PathVariable("name")String name){
        return name;
    }
}

