package com.fourthtimer.cmfz.controller;

import com.fourthtimer.cmfz.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("log")
public class LogController {
@Autowired
    private LogService logService;
    @RequestMapping("selectAllLog")
    public Map selectAllLog(int page,int limit,String name){
        System.out.println(logService.selectByPage(page,limit,name));
        return logService.selectByPage(page,limit,name);
    }
}
