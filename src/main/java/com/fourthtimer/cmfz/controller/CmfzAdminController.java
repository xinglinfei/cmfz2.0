package com.fourthtimer.cmfz.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ICaptcha;
import com.fourthtimer.cmfz.entity.Admin;
import com.fourthtimer.cmfz.service.AdminService;
import com.sun.tools.hat.internal.model.ReachableExcludesImpl;
import org.apache.catalina.security.SecurityUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

@Controller
@RequestMapping("admin")
public class CmfzAdminController {
    @Autowired
    private AdminService adminService;

    @RequestMapping("loginAdmin")
    public String adminLogin(String username, String password,String code) {
        //1、封装token
        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        //2、获取主体
        Subject subject = SecurityUtils.getSubject();

        Session session = subject.getSession();
        ICaptcha iCaptcha = (ICaptcha) session.getAttribute("ICaptcha");
        Boolean verify = iCaptcha.verify(code);
        System.out.println(verify);
        if (verify) {
            //3、认证
            try {
                subject.login(token);
                System.out.println("登录成功");
                return "redirect:/manage.jsp";
            } catch (AuthenticationException e) {
                System.out.println("登录失败");
                return "redirect:/manage.jsp";
            }
        }else{
            System.out.println("验证码错误");
            return "redirect:/manage.jsp";
        }
    }

/*    @RequestMapping("loginAdmin")
    public String adminLogin(String username, String password, String code, HttpSession session) {
        //从验证码拿到验证码对象
        ICaptcha iCaptcha = (ICaptcha) session.getAttribute("ICaptcha");
        //通过验证码对象的方法    做验证码校验  code是页面接收到的验证码  verify方法可以自动忽略大小写
        Boolean verify = iCaptcha.verify(code);
        System.out.println(verify);
        if (verify) {
            Boolean login = adminService.login(username, password);
            if (login) {
                System.out.println("成功");
                return "redirect:/manage.jsp";
            }
        }
        System.out.println("失败");
        return "redirect:/login.jsp";
    }*/

    @RequestMapping("getCaptcha")
    public void getCaptcha(HttpServletResponse response, HttpSession session) throws IOException {
        //定义验证码的长和宽 生成验证码对象
        ICaptcha lineCaptcha = CaptchaUtil.createCircleCaptcha(200, 100, 4, 20);
        //获取生成验证码中的值
        String code = lineCaptcha.getCode();
        System.out.println("验证码" + code);
        //把验证码对象放入到session中
        session.setAttribute("ICaptcha", lineCaptcha);
        //写出到浏览器
        lineCaptcha.write(response.getOutputStream());
    }

    @RequestMapping("selectAllAdmin")
    @ResponseBody
    public Map selectAllAdmin(int page, int limit) {
        return adminService.selectByPage(page, limit);
    }

    @RequestMapping("addAdmin")
    @ResponseBody
    public Map addAdmin(Admin admin) {
        return adminService.insert(admin);
    }

    @RequestMapping("deleteAdmin")
    @ResponseBody
    public Map deleteAdmin(int id) {
        int[] ids = new int[]{id};
        return adminService.delete(ids);
    }

    @RequestMapping("multiDeleteAdmin")
    @ResponseBody
    public Map multiDeleteAdmin(int[] ids) {
        return adminService.delete(ids);
    }

    @RequestMapping("updateAdmin")
    @ResponseBody
    public Map updateAdmin(Admin admin) {
        return adminService.update(admin);
    }

    @RequestMapping("selectOneAdmin")
    @ResponseBody
    public Admin selectOneAdmin(int id) {
        return adminService.selectOneAdmin(id);
    }
}
