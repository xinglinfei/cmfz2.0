package com.fourthtimer.cmfz.controller;

import com.fourthtimer.cmfz.annotation.LogAnnotation;
import com.fourthtimer.cmfz.entity.User;
import com.fourthtimer.cmfz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("selectAllUser")
    public Map selectAllUser(int page, int limit) {
        return userService.selectAll(page, limit);
    }

    @RequestMapping("addUser")
    public Map addUser(User user, MultipartFile touXiang, HttpServletRequest request) throws IOException {
        System.out.println("添加" + user + "头像" + touXiang);
        String realPath = request.getSession().getServletContext().getRealPath("image");
        String newFileName = new Date().getTime() + "_" + touXiang.getOriginalFilename();
        touXiang.transferTo(new File(realPath + "/" + newFileName));
        user.setUserImage(newFileName);
        return userService.insert(user);
    }

    @RequestMapping("deleteUser")
    public Map deleteUser(int userId) {
        int[] userIds = new int[]{userId};
        return userService.delete(userIds);
    }

    @RequestMapping("multiDeleteUser")
    public Map multiDeleteUser(int[] userIds) {
        System.out.println("批量删除用户" + userIds);
        return userService.delete(userIds);
    }

    @RequestMapping("updateUser")
    public Map updateUser(User user) {
        return userService.update(user);
    }

    @RequestMapping("selectOneUser")
    public User selectOneUser(int userId) {
        return userService.selectOneUser(userId);
    }

    @RequestMapping("getSexCount")
    @LogAnnotation(type="select",value="获取男女人数")
    public List<Map> getSexCount() {
        return userService.selectBySexCount();
    }

    @RequestMapping("getProvinceCount")
    @LogAnnotation(type="select",value="获取省份数据")
    public List<Map> getProvinceCount() {
        return userService.selectByProvince();
    }

    @RequestMapping("getByDayCount")
    @LogAnnotation(type="select",value="获取注册量数据")
    public Map getByDayCount() {
        return userService.selectByDayCount();
    }

    /**
     * 优化
     * @return
     * @throws InterruptedException
     */
    @RequestMapping("selectAllCountThread")
    @LogAnnotation(type="select",value="优化版获取用户信息")
    public Map selectAllCountThread() throws InterruptedException {
        long start = System.currentTimeMillis();
        Map map = userService.selectByThread();
        long end = System.currentTimeMillis();
        System.out.println(end-start+"ms");
        return map;
    }
}

