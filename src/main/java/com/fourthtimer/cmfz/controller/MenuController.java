package com.fourthtimer.cmfz.controller;

import com.fourthtimer.cmfz.annotation.LogAnnotation;
import com.fourthtimer.cmfz.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("menu")
public class MenuController {
    @Autowired
    private MenuService menuService;
    @RequestMapping("selectAll")
    //自定义注解
    @LogAnnotation(type="select",value="查询所有的菜单")
    public List selectAll(){
        return menuService.selectAll();
    }
}
