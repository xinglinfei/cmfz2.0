package com.fourthtimer.cmfz.controller;

import com.fourthtimer.cmfz.entity.Article;
import com.fourthtimer.cmfz.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @RequestMapping("selectAllArticle")
    public Map selectAllArticle(int page,int limit){
        return articleService.selectAll(page,limit);
    }

    @RequestMapping("addArticle")
    public Map addArticle(Article article){
        return articleService.insert(article);
    }

    @RequestMapping("deleteArticle")
    public Map deleteArticle(int articleId){
        int[] articleIds=new int[]{articleId};
        return articleService.delete(articleIds);
    }

    @RequestMapping("multiDeleteArticle")
    public Map multiDeleteArticle(int[] articleIds){
        return articleService.delete(articleIds);
    }

    @RequestMapping("updateArticle")
    public Map updateArticle(Article article){
        return articleService.insert(article);
    }

    @RequestMapping("selectOneArticle")
    public Article selectOneArticle(int articleId){
        return articleService.selectOneArticle(articleId);
    }
}
