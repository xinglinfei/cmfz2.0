package com.fourthtimer.cmfz.controller;

import com.fourthtimer.cmfz.entity.Guru;
import com.fourthtimer.cmfz.service.GuruService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("guru")
public class GuruController {
    @Autowired
    private GuruService guruService;
    @RequestMapping("selectAll")
    public Map selectAll(int page,int limit,String name){
        return guruService.selectByPage(page,limit,name);
    }

    @RequestMapping("add")
    public Map add(Guru guru, MultipartFile touXiang, HttpServletRequest request) throws IOException {
        System.out.println("添加"+guru+"头像"+touXiang);
        String realPath = request.getSession().getServletContext().getRealPath("image");
        String newFileName = new Date().getTime()+"_"+touXiang.getOriginalFilename();
        touXiang.transferTo(new File(realPath+"/"+newFileName));
        guru.setGuruImage(newFileName);
        return guruService.insert(guru);
    }

    @RequestMapping("delete")
    public Map delete(int guruId){
        System.out.println("删除"+guruId);
//        int[] guruIds = new int[]{guruId};
        int[] guruIds = new int[1];
        guruIds[0]=guruId;
        System.out.println("删除"+guruIds);
        return guruService.delete(guruIds);
    }

    @RequestMapping("multiDelete")
    public Map multiDelete(int[] guruIds){
        System.out.println("批量删除"+guruIds);
        System.out.println("收到的数组："+guruIds);
        return guruService.delete(guruIds);
    }

    @RequestMapping("selectOneGuru")
    public Guru selectOneGuru(int guruId){
        System.out.println("单查"+guruId);
        return guruService.selectOneGuru(guruId);
    }
    @RequestMapping("update")
    public Map update(Guru guru){
        System.out.println("修改"+guru);
        return guruService.update(guru);
    }
    @RequestMapping("recover")
    public Map recover(int guruId){
        System.out.println("恢复"+guruId);
        return guruService.recover(guruId);
    }

    @RequestMapping("multiDownload")
    public void multiDownload(HttpServletResponse response) throws Exception {
        System.out.println("批量导出");
        guruService.multiDownload(response);
    }

    @RequestMapping("multiUpload")
    public Map multiUpload(MultipartFile file) throws Exception {
        System.out.println("批量导入数据"+file);
        Map map = new HashMap();
        try {
            guruService.multiUpload(file);
            map.put("idUpload",true);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("idUpload",false);
        }
        return map;
    }
}
