package com.fourthtimer.cmfz.controller;

import com.fourthtimer.cmfz.entity.Banner;
import com.fourthtimer.cmfz.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("banner")
public class BannerController {
    @Autowired
    private BannerService bannerService;
    @RequestMapping("selectAllBanner")
    public Map selectAllBanner(int page, int limit){
        return bannerService.selectAll(page,limit);
    }

    @RequestMapping("addBanner")
    public Map addBanner(Banner banner){
        return bannerService.insert(banner);
    }

    @RequestMapping("multiDeleteBanner")
    public Map multiDeleteBanner(int[] bannerIds){
        return bannerService.delete(bannerIds);
    }

    @RequestMapping("deleteBanner")
    public Map deleteBanner(int bannerId){
        int[] bannerIds = new int[]{bannerId};
        return bannerService.delete(bannerIds);
    }

    @RequestMapping("updateBanner")
    public Map updateBanner(Banner banner){
        return bannerService.update(banner);
    }


    @RequestMapping("selectOneBanner")
    public Banner selectOneBanner(int bannerId){
        return bannerService.selectOneBanner(bannerId);
    }
}
