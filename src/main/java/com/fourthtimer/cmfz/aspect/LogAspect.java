package com.fourthtimer.cmfz.aspect;

import com.fourthtimer.cmfz.annotation.LogAnnotation;
import com.fourthtimer.cmfz.dao.LogDao;
import com.fourthtimer.cmfz.entity.Admin;
import com.fourthtimer.cmfz.entity.Log;
import com.fourthtimer.cmfz.util.IPKit;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.annotation.Annotation;
import java.util.Date;

/**
 * 切面类
 *
 * @Aspect 定义当前类为切面类    可以认为这个注解会自动的编织增强和切点
 */
@Aspect
@Configuration
public class LogAspect {
    @Autowired
    private LogDao logDao;
    /**
     * 1、定义切入点   a:切方法 b:切注解
     * @Pointcut 定义切入点
     * 参数为切点表达式
     * 切点的名字就是方法的名字+()例如：logPoint()
     * 注意：注解下面这个方法随便写(注解需要加载方法)
     */
    @Pointcut("@annotation(com.fourthtimer.cmfz.annotation.LogAnnotation)")
    public void logPoint(){

    }
    /**
     * 2、定义增强
     *
     * 参数为  切点的名字
     * 参数也可以是切点表达式：execution(* com.fourthtimer.cmfz.service.*.*(..))
     * 切点的设置要设置为切注解
     *  原因：
     *      1、要获取注解的内容
     *      2、切注解更加灵活
     * JoinPoint可以获取切入点附近的数据
     */

    @After("logPoint()")
    public void logAfter(JoinPoint joinPoint){
        //1、获取信息
        //1.1、采集日志信息
        Log log = new Log();
        //1.2、时间
        log.setLogDate(new Date());
        //1.3ip获取 从request中获取
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String ipAddrByRequest = IPKit.getIpAddrByRequest(request);
        log.setLogIp(ipAddrByRequest);
        //1.4、管理员的名字的获取
        HttpSession session = request.getSession();
        Admin admin = (Admin) session.getAttribute("admin");
        System.out.println(admin);
        log.setLogAdminName(admin.getUsername());
//        log.setLogAdminName("zhangsan");
        /*
         * 1.5、获取类型和操作内容 难点：执行的方法不一样 类型和操作内容也不一样
         * 通过自定义注解的方式   ctrl+h可以查看继承和实现的关系
         */
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        //获取方法对象    方法的注解
        Annotation annotation = signature.getMethod().getAnnotation(LogAnnotation.class);
        log.setLogType(((LogAnnotation) annotation).type());
        log.setLogContent(((LogAnnotation) annotation).value());
        System.out.println(log+"插入的日志信息");
        //、2将日志信息添加到数据库
        logDao.insert(log);
    }
}
