package com.fourthtimer.cmfz.aspect;

import com.sun.tools.jdi.EventSetImpl;
import io.lettuce.core.ScriptOutputType;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Aspect
@Configuration
public class CacheAspect {
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 删除缓存
     */
    @Around("@annotation(com.fourthtimer.cmfz.annotation.AddCacheAnnotation)")
    public Object addCache(ProceedingJoinPoint joinPoint) throws Throwable {
//        1、通过类名查询缓存中是否有数据
//        获取key
        String className = joinPoint.getTarget().getClass().getName();
        System.out.println("className"+className);
//       获取hashkey
//        获取方法名
        String methodName = joinPoint.getSignature().getName();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(methodName);
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            stringBuffer.append(arg);
        }
        String hashKey = stringBuffer.toString();
        System.out.println("hashKey"+hashKey);
//        通过className和hashKey获取缓存数据
        HashOperations hashOperations = redisTemplate.opsForHash();
        Object result = hashOperations.get(className, hashKey);
//        2、如果有数据，直接返回缓存中存取的数据
//        3、如果没有数据,查询数据获取数据库的响应,添加缓存并返回
        if (result==null) {
            System.out.println("查询数据库");
            //查询数据库 放行操作    类似于invoke代表执行当前被切的方法
            result = joinPoint.proceed();
            //添加缓存
            Map map = new HashMap();
            map.put(hashKey,result);
            hashOperations.putAll(className,map);
            /**
             * 设置失效时间 让缓存自动过期
             */
            redisTemplate.expire(className,10, TimeUnit.SECONDS);
        }else{
            System.out.println("缓存取值");
        }
        return result;
    }
    @Before("@annotation(com.fourthtimer.cmfz.annotation.DeleteCacheAnnotation)")
    public void deleteCache(JoinPoint joinPoint){
        //删除缓存  key是谁?
         Object target = joinPoint.getTarget();
         String className = target.getClass().getName();
        System.out.println(target.getClass().getName());
        System.out.println(target);
    }
}
