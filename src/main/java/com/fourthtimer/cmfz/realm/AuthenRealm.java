package com.fourthtimer.cmfz.realm;

import com.fourthtimer.cmfz.dao.AdminDao;
import com.fourthtimer.cmfz.entity.Admin;
import com.fourthtimer.cmfz.service.AdminService;
import jdk.nashorn.internal.parser.Token;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.springframework.beans.factory.annotation.Autowired;

public class AuthenRealm extends AuthenticatingRealm {
    @Autowired
    private AdminDao adminDao;

    /**
     * doGetAuthenticationInfo  获取认证信息
     *
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //1、获取用户输入的帐号
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        String username = usernamePasswordToken.getUsername();
        //2、查询数据库 通过用户名查询
        Admin admin = adminDao.selectOneAdminByUsername(username);
        //2.1、如果数据库查询返回null直接返回null 就会报帐号不存在
        //2.2、如果不为空 返回一个实现类(info)对象
        if (admin != null) {
            SecurityUtils.getSubject().getSession().setAttribute("admin",admin);
            /**
             * 参数1 帐号
             * 参数2 密码
             * 参数3 this.getName()
             */
            return new SimpleAuthenticationInfo(admin.getUsername(),admin.getPassword(),this.getName());
        }
        return null;
    }
}
