package com.fourthtimer.cmfz.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 元注解：注解的注解(加在自定义注解上的注解)
 * @Target 定义了当前注解可以加在什么位置 类还是方法 method可以加在方法上
 * @Retention 定义作用范围
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LogAnnotation {
    /**
     * 定义注解的属性
     * 可以给默认值
     * @return
     */
    String type() default "";

    String value() default "";
}
