<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <title>管理员管理</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script src="${pageContext.request.contextPath}/layui/layui.js"></script>
    <link href="${pageContext.request.contextPath}/layui/css/layui.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        //加载要用到的模块
        layui.use(['table', 'jquery', 'layer', 'form', 'upload', 'element'], function () {
            var table = layui.table;
            var $ = layui.jquery;
            var layer = layui.layer;
            var form = layui.form;
            var upload = layui.upload;
            var element = layui.element;

            table.render({
                elem: "#myTable"
                ,url: '${pageContext.request.contextPath}/admin/selectAllAdmin'
                ,page: true//开启分页
                ,toolbar: '#myToolBar'//开启工具条
                ,cols: [[
                    {type:'checkbox',rowspan:2}
                    ,{type:'numbers',rowspan:2}
                    ,{field:'id',title:'管理员编号',sort:'true',rowspan:2}
                    ,{field:'username',title:'管理员名称',rowspan:2}
                    ,{field:'password',title:'管理员密码',rowspan:2}
                    , {title: '操作', rowspan: 2, align: 'center', templet: '#myOperateTemplet'}
                ]]
            });
            // 监听工具栏事件
            var layerIndex = 0;
            table.on("toolbar(myTableFilter)", function (obj) {
                // console.log(obj);
                switch (obj.event) {
                    case 'add':
                        //添加
                        //1.通过弹出层显示添加输入框
                        layerIndex = layer.open({type: 1, content: $("#insertForm")});

                        break;
                    case 'multiDelete':
                        //批量删除
                        //1.获取到选中的数据
                        var selectRows = table.checkStatus("myTable");
                        console.log("批量删除" + selectRows);
                        if (selectRows.data.length == 0) {
                            //提示    还没有数据被选中
                            layer.msg("请选中要删除的行", {icon: 4});
                        } else {
                            layer.confirm("真的删除选中的所有行,不可恢复?", {title: '删除确认框', icon: 3}, function (index) {
                                //点击确认的时候会进到这个函数
                                //关闭此确认框
                                layer.close(index);
                                var guruIds = new Array();
                                $.each(selectRows.data, function (index, obj) {
                                    guruIds[index] = obj.guruId;
                                });
                                //发送ajax请求
                                $.ajax({
                                    url: "${pageContext.request.contextPath}/guru/multiDelete"
                                    , data: "guruIds=" + guruIds
                                    , type: "post"
                                    , success: function (data) {
                                        if (data.isDelete) {
                                            layer.msg("删除成功", {icon: 6});
                                            //删除成功需要刷新Table
                                            table.reload("myTable");
                                        } else {
                                            layer.msg("删除失败", {icon: 1});
                                        }
                                    }
                                });
                            });
                        }
                        break;

                    case 'multiDownload':
                        //批量导出
                        //获取到全部表格数据
                        var selectRows = table.checkStatus("myTable");
                        console.info("批量导出"+selectRows);
                        $.ajax({
                            url: "${pageContext.request.contextPath}/guru/multiDownload"
                        });
                        break;
                    case 'search':
                        //1.获取到页面输入框输入的内容
                        //2.发送ajax请求到后台,执行查询操作,然后刷新(reload)table
                        var name = $("#searchName").val();
                        // alert(name+"哈哈哈哈哈");

                        //刷新表单  重新加载
                        table.reload("myTable", {
                            "where": {"name": name}
                        })
                }
            });
            // 上传文件
            upload.render({
                elem: '#touXiang'
                , auto: false
                , field: 'touXiang'
                , progress: function (n) {
                    var percent = n + '%' //获取进度百分比
                    element.progress('demo', percent); //可配合 layui 进度条元素使用
                }
                , choose: function (obj) {
                    obj.preview(function (index, file, result) {
                        console.info(obj);
                        $("#myTouXiang").attr("src", result);
                    });
                }
            });

            form.on("submit(insertFormFilter)", function (obj) {
                //form对象 复合表单   jQuery选择器转DOM元素
                var formData = new FormData($("#insertForm")[0]);
                console.log(formData);
                //关闭添加弹出层
                layer.close(layerIndex);
                //发送ajax请求到后台,执行添加操作
                $.ajax({
                    url: "${pageContext.request.contextPath}/guru/add"
                    , data: formData
                    , contentType: false
                    , processData: false
                    , async: false
                    , type: "post"
                    , success: function (data) {
                        //根据后台响应回来的数据做相应的处理
                        if (data.isInsert) {
                            layer.msg("添加成功", {icon: 6});
                            table.reload("myTable");
                        } else {
                            layer.msg("添加失败", {icon: 1});
                        }
                    }
                })
                return false;
            });

            //监听工具条事件
            table.on("tool(myTableFilter)", function (obj) {
                var data = obj.data;//获取当前行数据
                console.info(data.guruId);
                switch (obj.event) {
                    case 'edit':
                        //回填数据
                        $("#guruId").val(data.guruId);
                        $("#guruName").val(data.guruName);
                        // $("#guruImage").val(data.guruImage);
                        $("#guruNickName").val(data.guruNickName);
                        var guruStatus = data.guruStatus;
                        if (guruStatus == 1) {
                            $("#dongjie").prop("checked", true);
                        } else {
                            $("#zhengchang").prop("checked", true);
                        }
                        //重新渲染
                        form.render();
                        //通过弹出层显示修改输入框
                        layerIndex = layer.open({type: 1, content: $("#updateForm")});
                        break;

                    case 'del':
                        //提示确认删除框
                        layer.confirm("真的删除这一行吗？不可恢复", {title: '删除确认框', icon: 3}, function (index) {
                            //点击确认进入此函数
                            //关闭确认框
                            layer.close(index);
                            //发送ajax请求
                            $.ajax({
                                url: "${pageContext.request.contextPath}/guru/delete"
                                , data: "guruId=" + obj.data.guruId
                                , type: 'post'
                                , success: function (data) {
                                    if (data.isDelete) {
                                        // obj.del();
                                        //提示信息
                                        layer.msg("删除成功", {icon: 6});
                                        table.reload("myTable");
                                    } else {
                                        layer.msg("删除失败", {icon: 1});
                                    }
                                }
                            });
                        });
                        //避免顺序执行  强制结束
                        break;
                    // return 0;

                    case 'recover':
                        //收guruId
                        $.ajax({
                            url: "${pageContext.request.contextPath}/guru/recover"
                            , data: "guruId=" + obj.data.guruId
                            , type: 'post'
                            , success: function (data) {
                                if (data.isRecover) {
                                    layer.msg("恢复成功", {icon: 6});
                                    table.reload("myTable");
                                } else {
                                    layer.msg("恢复成功", {icon: 6});
                                }
                            }
                        });
                }
            });
            form.on("submit(updateFormFilter)", function (obj) {
                layer.close(layerIndex);
                $.ajax({
                    url: '${pageContext.request.contextPath}/guru/update'
                    , data: obj.field
                    , type: 'post'
                    , success: function (data) {
                        if (data.isUpdate) {
                            layer.msg("修改成功", {icon: 6});
                            table.reload("myTable");
                        } else {
                            layer.msg("修改失败", {icon: 6});
                        }
                    }
                });
                return false;
            });
        });
    </script>

</head>
<script type="text/html" id="myToolBar">
    <div class="layui-btn-container layui-inline">
        <button class="layui-btn layui-btn-normal" lay-event="add">添加</button>
        <button class="layui-btn layui-btn-normal" lay-event="multiDelete">批量删除</button>
        <button class="layui-btn layui-btn-normal" lay-event="multiDownload">批量下载</button>
        <button class="layui-btn layui-btn-normal" lay-event="multiupload">批量上传</button>
    </div>
    <%--搜索功能对应的页面效果--%>
<%--    <div class="layui-inline">
        <label class="layui-form-label">用户名</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" id="searchName" name="name" placeholder="关键字" class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px">
            <input type="button" class="layui-btn layui-btn-normal" lay-event="search" value="搜索">
        </div>
    </div>--%>
</script>


<script type="text/html" id="myOperateTemplet">
    <a class="layui-btn layui-btn-sm" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="del">删除</a>
</script>


<body>
<table id="myTable" lay-filter="myTableFilter"></table>

<%--添加的表单--%>
<form class="layui-form" id="insertForm" style="display:none;" enctype="multipart/form-data">
    <input name="guruId" type="hidden"/>
    <div class="layui-form-item">
        <label class="layui-form-label">上师姓名</label>
        <div class="layui-input-block">
            <input type="text" name="guruName" required lay-verify="required" placeholder="请输入上师名" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">上师头像</label>
        <div class="layui-input-block">
            <button type="button" class="layui-btn" id="touXiang">
                <i class="layui-icon">&#xe67c;</i>选择要上传的头像
            </button>
            <img src="" id="myTouXiang" alt="请选择图片" style="height: 100px; width: 100px"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">上师法名</label>
        <div class="layui-input-inline">
            <input type="password" name="guruNickName" required lay-verify="required" placeholder="请输入法名"
                   autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <%--    <div class="layui-form-item">
            <label class="layui-form-label">账号状态</label>
            <div class="layui-input-block">
                <input type="radio" name="guruId" value="1"  title="冻结">
                <input type="radio" name="guruId" value="0"  title="正常">
            </div>
        </div>--%>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="insertFormFilter">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<%--修改的表单--%>
<form class="layui-form" id="updateForm" style="display:none;">
    <input name="id" id="id" type="hidden"/>
    <div class="layui-form-item">
        <label class="layui-form-label">用户名</label>
        <div class="layui-input-block">
            <input type="text" name="name" id="name" required lay-verify="required" placeholder="请输入用户名"
                   autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码框</label>
        <div class="layui-input-inline">
            <input type="password" name="password" id="password" required lay-verify="required" placeholder="请输入密码"
                   autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">性别</label>
        <div class="layui-input-block">
            <input type="radio" name="gender" value="1" id="genderMale" title="男">
            <input type="radio" name="gender" value="0" id="genderFemale" title="女">
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="updateFormFilter">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
</body>
</html>