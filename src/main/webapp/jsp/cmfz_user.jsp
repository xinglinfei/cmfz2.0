<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <title>用户管理</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script src="${pageContext.request.contextPath}/layui/layui.js"></script>
    <link href="${pageContext.request.contextPath}/layui/css/layui.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        //加载要用到的模块
        layui.use(['table', 'jquery', 'layer', 'form', 'upload', 'element'], function () {
            var table = layui.table;
            var $ = layui.jquery;
            var layer = layui.layer;
            var form = layui.form;
            var upload = layui.upload;
            var element = layui.element;

            table.render({
                elem: "#myTable"
                , url: '${pageContext.request.contextPath}/user/selectAllUser'
                , page: true//开启分页
                , toolbar: '#myToolBar'//开启工具条
                , cols: [[
                    {type: 'checkbox'}
                    , {type: 'numbers'}
                    , {field: 'userId', title: '用户编号', sort: 'true'}
                    , {field: 'userTelephone', title: '用户电话'}
                    , {field: 'userPassword', title: '用户密码'}
                    , {field: 'userImage', title: '用户头像'}
                    , {field: 'userNickName', title: '用户昵称'}
                    , {field: 'userName', title: '用户真名',}
                    , {field: 'userSex', title: '用户性别', templet: '#mySexTemplet'}
                    , {field: 'userAutograph', title: '个人简介'}
                    , {field: 'userStatus', title: '用户状态', templet: '#myStatusTemplet'}
                    , {field: 'userCreateDate', title: '注册日期', templet: '#myDateTemplet'}
                    , {field: 'guru.guruId', title: '关注的人', templet: '#myGuruTemplet'}
                    , {field: 'userProvince', title: '省', align: 'center'}
                    , {field: 'userCity', title: '市', align: 'center'}
                    , {field: 'userStatus', title: '操作', align: 'center', templet: '#myOperateTemplet'}]]
            });
            // 监听工具栏事件
            var layerIndex = 0;
            table.on("toolbar(myTableFilter)", function (obj) {
                // console.log(obj);
                switch (obj.event) {
                    case 'add':
                        //添加
                        //1.通过弹出层显示添加输入框
                        layerIndex = layer.open({type: 1, content: $("#insertForm")});

                        break;
                    case 'multiDelete':
                        //批量删除
                        //1.获取到选中的数据
                        var selectRows = table.checkStatus("myTable");
                        console.log("批量删除" + selectRows);
                        if (selectRows.data.length == 0) {
                            //提示    还没有数据被选中
                            layer.msg("请选中要删除的行", {icon: 4});
                        } else {
                            layer.confirm("真的删除选中的所有行,不可恢复?", {title: '删除确认框', icon: 3}, function (index) {
                                //点击确认的时候会进到这个函数
                                //关闭此确认框
                                layer.close(index);
                                var userIds = new Array();
                                $.each(selectRows.data, function (index, obj) {
                                    userIds[index] = obj.userId;
                                });
                                //发送ajax请求
                                $.ajax({
                                    url: "${pageContext.request.contextPath}/user/multiDeleteUser"
                                    , data: "userIds=" + userIds
                                    , type: "post"
                                    , success: function (data) {
                                        if (data.isDelete) {
                                            layer.msg("删除成功", {icon: 6});
                                            //删除成功需要刷新Table
                                            table.reload("myTable");
                                        } else {
                                            layer.msg("删除失败", {icon: 1});
                                        }
                                    }
                                });
                            });
                        }
                        break;

                    case 'multiDownload':
                        //批量导出
                        //获取到全部表格数据
                        var selectRows = table.checkStatus("myTable");
                        console.info("批量导出"+selectRows);
                        $.ajax({
                            url: "${pageContext.request.contextPath}/user/multiDownloadUser"
                        });
                        break;
                    case 'search':
                        //1.获取到页面输入框输入的内容
                        //2.发送ajax请求到后台,执行查询操作,然后刷新(reload)table
                        var name = $("#searchName").val();
                        // alert(name+"哈哈哈哈哈");

                        //刷新表单  重新加载
                        table.reload("myTable", {
                            "where": {"name": name}
                        })
                }
            });
            // 上传文件
            upload.render({
                elem: '#touXiang'
                , auto: false
                , field: 'touXiang'
                , progress: function (n) {
                    var percent = n + '%' //获取进度百分比
                    element.progress('demo', percent); //可配合 layui 进度条元素使用
                }
                , choose: function (obj) {
                    obj.preview(function (index, file, result) {
                        console.info(obj);
                        $("#myTouXiang").attr("src", result);
                    });
                }
            });

            form.on("submit(insertFormFilter)", function (obj) {
                //form对象 复合表单   jQuery选择器转DOM元素
                var formData = new FormData($("#insertForm")[0]);
                console.log(formData);
                //关闭添加弹出层
                layer.close(layerIndex);
                //发送ajax请求到后台,执行添加操作
                $.ajax({
                    url: "${pageContext.request.contextPath}/user/addUser"
                    , data: formData
                    , contentType: false
                    , processData: false
                    , async: false
                    , type: "post"
                    , success: function (data) {
                        //根据后台响应回来的数据做相应的处理
                        if (data.isInsert) {
                            layer.msg("添加成功", {icon: 6});
                            table.reload("myTable");
                        } else {
                            layer.msg("添加失败", {icon: 1});
                        }
                    }
                })
                return false;
            });

            //监听工具条事件
            table.on("tool(myTableFilter)", function (obj) {
                var data = obj.data;//获取当前行数据
                console.info(data.userId);
                switch (obj.event) {
                    case 'edit':
                        //回填数据
                        $("#userId").val(data.userId);
                        $("#userName").val(data.userName);
                        $("#userTelephone").val(data.userTelephone);
                        $("#userAutograph").val(data.userAutograph);
                        $("#userProvince").val(data.userProvince);
                        $("#userCity").val(data.userCity);
                        // $("#guruImage").val(data.guruImage);
                        $("#userNickName").val(data.userNickName);
                        var userSex = data.userSex;
                        if (userId == 1) {
                            $("#genderMale").prop("checked", true);
                        } else {
                            $("#genderFemale").prop("checked", true);
                        }
                        //重新渲染
                        form.render();
                        //通过弹出层显示修改输入框
                        layerIndex = layer.open({type: 1, content: $("#updateForm")});
                        break;

                    case 'del':
                        //提示确认删除框
                        layer.confirm("真的删除这一行吗？不可恢复", {title: '删除确认框', icon: 3}, function (index) {
                            //点击确认进入此函数
                            //关闭确认框
                            layer.close(index);
                            //发送ajax请求
                            $.ajax({
                                url: "${pageContext.request.contextPath}/user/deleteUser"
                                , data: "userId=" + obj.data.userId
                                , type: 'post'
                                , success: function (data) {
                                    if (data.isDelete) {
                                        // obj.del();
                                        //提示信息
                                        layer.msg("删除成功", {icon: 6});
                                        table.reload("myTable");
                                    } else {
                                        layer.msg("删除失败", {icon: 1});
                                    }
                                }
                            });
                        });
                        //避免顺序执行  强制结束
                        break;
                    // return 0;

                    case 'recover':
                        //收guruId
                        $.ajax({
                            url: "${pageContext.request.contextPath}/user/recoverUser"
                            , data: "userId=" + obj.data.userId
                            , type: 'post'
                            , success: function (data) {
                                if (data.isRecover) {
                                    layer.msg("恢复成功", {icon: 6});
                                    table.reload("myTable");
                                } else {
                                    layer.msg("恢复成功", {icon: 6});
                                }
                            }
                        });
                }
            });
            form.on("submit(updateFormFilter)", function (obj) {
                layer.close(layerIndex);
                $.ajax({
                    url: '${pageContext.request.contextPath}/user/updateUser'
                    , data: obj.field
                    , type: 'post'
                    , success: function (data) {
                        if (data.isUpdate) {
                            layer.msg("修改成功", {icon: 6});
                            table.reload("myTable");
                        } else {
                            layer.msg("修改失败", {icon: 6});
                        }
                    }
                });
                return false;
            });
        });
    </script>


</head>
<script type="text/html" id="myToolBar">
    <div class="layui-btn-container layui-inline">
        <button class="layui-btn layui-btn-normal" lay-event="add">添加</button>
        <button class="layui-btn layui-btn-normal" lay-event="multiDelete">批量删除</button>
        <button class="layui-btn layui-btn-normal" lay-event="multiDownload">批量下载</button>
        <button class="layui-btn layui-btn-normal" lay-event="multiupload">批量上传</button>
    </div>
    <%--搜索功能对应的页面效果--%>
    <%--    <div class="layui-inline">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" id="searchName" name="name" placeholder="关键字" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px">
                <input type="button" class="layui-btn layui-btn-normal" lay-event="search" value="搜索">
            </div>
        </div>--%>
</script>


<script type="text/html" id="myOperateTemplet">
    <a class="layui-btn layui-btn-sm" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="del">删除</a>
</script>

<script type="text/html" id="myStatusTemplet">
    {{#  if(d.userStatus == 1){ }}
    已注销
    {{#  } else{ }}
    正常使用
    {{#  } }}
</script>

<script type="text/html" id="myDateTemplet">
    {{layui.util.toDateString(d.userCreateDate, 'yyyy-MM-dd')}}
</script>

<script type="text/html" id="mySexTemplet">
    {{#  if(d.userSex == 1){ }}
    女
    {{#  } else{ }}
    男
    {{#  } }}
</script>

<script type="text/html" id="myGuruTemplet">
    {{d.guru.guruNickName}}
</script>

<body>
<table id="myTable" lay-filter="myTableFilter"></table>

<%--添加的表单--%>
<form class="layui-form" id="insertForm" style="display:none;" enctype="multipart/form-data">
    <input name="userId" type="hidden"/>
    <div class="layui-form-item">
        <label class="layui-form-label">用户姓名</label>
        <div class="layui-input-block">
            <input type="text" name="userName" required lay-verify="required" placeholder="请输入用户姓名" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户电话</label>
        <div class="layui-input-block">
            <input type="text" name="userTelephone" required lay-verify="required" placeholder="请输入用户电话" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户密码</label>
        <div class="layui-input-block">
            <input type="password" name="userPassword" required lay-verify="required" placeholder="请输入用户密码" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户头像</label>
        <div class="layui-input-block">
            <button type="button" class="layui-btn" id="touXiang">
                <i class="layui-icon">&#xe67c;</i>选择要上传的头像
            </button>
            <img src="" id="myTouXiang" alt="请选择图片" style="height: 100px; width: 100px"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户昵称</label>
        <div class="layui-input-inline">
            <input type="text" name="userNickName" required lay-verify="required" placeholder="用户昵称"
                   autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户性别</label>
        <div class="layui-input-block">
                <input type="radio" name="userSex" value="1"  title="女">
                <input type="radio" name="userSex" value="0"  title="男">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">个人简介</label>
        <div class="layui-input-block">
            <input type="text" name="userAutograph" required lay-verify="required" placeholder="请输入个人简介" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">籍贯</label>
        <div class="layui-input-block">
            <input type="text" name="userProvince" required lay-verify="required" placeholder="请输入省" autocomplete="off"
                   class="layui-input">
            <input type="text" name="userCity" required lay-verify="required" placeholder="请输入市" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <%--    <div class="layui-form-item">
            <label class="layui-form-label">账号状态</label>
            <div class="layui-input-block">
                <input type="radio" name="guruId" value="1"  title="冻结">
                <input type="radio" name="guruId" value="0"  title="正常">
            </div>
        </div>--%>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="insertFormFilter">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<%--修改的表单--%>
<form class="layui-form" id="updateForm" style="display:none;">
    <input name="userId" id="userId" type="hidden"/>
    <div class="layui-form-item">
        <label class="layui-form-label">用户姓名</label>
        <div class="layui-input-block">
            <input type="text" name="userName" id="userName"required lay-verify="required" placeholder="请输入用户姓名" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户电话</label>
        <div class="layui-input-block">
            <input type="text" name="userTelephone" id="userTelephone" required lay-verify="required" placeholder="请输入用户电话" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户密码</label>
        <div class="layui-input-block">
            <input type="password" name="userPassword" id="userPassword" required lay-verify="required" placeholder="请输入用户密码" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
<%--    <div class="layui-form-item">
        <label class="layui-form-label">用户头像</label>
        <div class="layui-input-block">
            <button type="button" class="layui-btn" id="newTouXiang">
                <i class="layui-icon">&#xe67c;</i>选择要上传的头像
            </button>
            <img src="" id="myNewTouXiang" alt="请选择图片" style="height: 100px; width: 100px"/>
        </div>
    </div>--%>
    <div class="layui-form-item">
        <label class="layui-form-label">用户昵称</label>
        <div class="layui-input-inline">
            <input type="text" name="userNickName" id="userNickName" required lay-verify="required" placeholder="用户昵称"
                   autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户性别</label>
        <div class="layui-input-block">
            <input type="radio" name="userSex" id="genderMale" value="1"  title="女">
            <input type="radio" name="userSex" id="genderFemale" value="0"  title="男">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">个人简介</label>
        <div class="layui-input-block">
            <input type="text" name="userAutograph" id="userAutograph" required lay-verify="required" placeholder="请输入个人简介" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">籍贯</label>
        <div class="layui-input-block">
            <input type="text" name="userProvince" id="userProvince" required lay-verify="required" placeholder="请输入省" autocomplete="off"
                   class="layui-input">
            <input type="text" name="userCity" id="userCity" required lay-verify="required" placeholder="请输入市" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <%--    <div class="layui-form-item">
            <label class="layui-form-label">账号状态</label>
            <div class="layui-input-block">
                <input type="radio" name="guruId" value="1"  title="冻结">
                <input type="radio" name="guruId" value="0"  title="正常">
            </div>
        </div>--%>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="updateFormFilter">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
</body>
</html>