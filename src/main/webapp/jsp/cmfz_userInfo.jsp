<%@page isELIgnored="false" pageEncoding="UTF-8" contentType="text/html; UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css">
    <script src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
    <script src="${pageContext.request.contextPath}/layui/layui.all.js" charset="utf-8"></script>
    <script src="${pageContext.request.contextPath}/js/echarts.js" charset="utf-8"></script>
    <script src="${pageContext.request.contextPath}/js/china.js" charset="utf-8"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 28px;
        }
    </style>
</head>
<body>
<!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
<div id="main" style="width: 600px;height:400px;"></div>
<%--注册量变化--%>
<div id="main2" style="width: 600px;height:400px;"></div>
<%--中国地图--%>
<div id="china" style="width: 600px;height:400px;"></div>


<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例  图表对象
    var myChart = echarts.init(document.getElementById('main'));
    //注册量变化
    var myChart2 = echarts.init(document.getElementById('main2'));
    //初始化中国地区的图表对象
    var china = echarts.init(document.getElementById("china"));

    <%--$.ajax({--%>
    <%--url: '${pageContext.request.contextPath}/user/getSexCount',--%>
    <%--success: function (data) {--%>
    <%--console.info(data)--%>
    <%--var man = data[1].value;--%>
    <%--var women = data[0].value;--%>
    <%--// 指定图表的配置项和数据--%>
    <%--var option = {--%>
    <%--//图表的标题--%>
    <%--title: {--%>
    <%--text: '持明法洲男女人数对比'--%>
    <%--},--%>
    <%--tooltip: {},--%>
    <%--//legend 传奇 数学中 系列【图表】 在图表中展示的数据类型--%>
    <%--legend: {--%>
    <%--data: ['人数']--%>
    <%--},--%>
    <%--// x轴坐标--%>
    <%--xAxis: {--%>
    <%--data: ["男", "女"]--%>
    <%--},--%>
    <%--// y轴自动识别不需要写--%>
    <%--yAxis: {},--%>
    <%--// 数据  给x轴赋值--%>
    <%--series: [{--%>
    <%--// name 必须 和 legend中类型一样--%>
    <%--name: '人数',--%>
    <%--// type 定义图片的展示样式 bar柱状图 line折线图 per饼状图--%>
    <%--type: 'bar',--%>
    <%--// data x轴坐标对应的坐标值 必须和x轴坐标一一对应--%>
    <%--data: [man, women]--%>
    <%--}]--%>
    <%--};--%>

    <%--// 使用刚指定的配置项和数据显示图表。 将配置赋值给图表对象--%>
    <%--myChart.setOption(option);--%>

    <%--}--%>
    <%--});--%>


    <%--// 注册量变化的图表赋值--%>
    <%--$.ajax({--%>
    <%--url: '${pageContext.request.contextPath}/user/getByDayCount',--%>
    <%--success: function (data) {--%>
    <%--console.info(data)--%>
    <%--// 指定图表的配置项和数据--%>
    <%--var option = {--%>
    <%--//图表的标题--%>
    <%--title: {--%>
    <%--text: '持明法洲用户注册量变化'--%>
    <%--},--%>
    <%--tooltip: {},--%>
    <%--//legend 传奇 数学中 系列【图表】 在图表中展示的数据类型--%>
    <%--legend: {--%>
    <%--data: ['注册量']--%>
    <%--},--%>
    <%--// x轴坐标--%>
    <%--xAxis: {--%>
    <%--data: ["一周", "两周", "三周"]--%>
    <%--},--%>
    <%--// y轴自动识别不需要写--%>
    <%--yAxis: {},--%>
    <%--// 数据  给x轴赋值--%>
    <%--series: [{--%>
    <%--// name 必须 和 legend中类型一样--%>
    <%--name: '人数',--%>
    <%--// type 定义图片的展示样式 bar柱状图 line折线图 per饼状图--%>
    <%--type: 'line',--%>
    <%--// data x轴坐标对应的坐标值 必须和x轴坐标一一对应--%>
    <%--data: [data.one, data.two, data.three]--%>
    <%--}]--%>
    <%--};--%>

    <%--// 使用刚指定的配置项和数据显示图表。 将配置赋值给图表对象--%>
    <%--myChart2.setOption(option);--%>

    <%--}--%>
    <%--});--%>


    <%--// 中国的图表赋值--%>
    <%--$.ajax({--%>
    <%--url: '${pageContext.request.contextPath}/user/getProvinceCount',--%>
    <%--success: function (data) {--%>
    <%--// 响应的数据格式 就是List<Map>--%>
    <%--console.info(data)--%>
    <%--var option = {--%>
    <%--title: {--%>
    <%--text: '用户地区分布',--%>
    <%--left: 'center'--%>
    <%--},--%>
    <%--tooltip: {--%>
    <%--trigger: 'item'--%>
    <%--},--%>
    <%--legend: {--%>
    <%--orient: 'vertical',--%>
    <%--left: 'left',--%>
    <%--data: ['用户人数']--%>
    <%--},--%>
    <%--visualMap: {--%>
    <%--min: 0,--%>
    <%--max: 2500,--%>
    <%--left: 'left',--%>
    <%--top: 'bottom',--%>
    <%--text: ['高', '低'],           // 文本，默认为数值文本--%>
    <%--calculable: true--%>
    <%--},--%>
    <%--toolbox: {--%>
    <%--show: true,--%>
    <%--orient: 'vertical',--%>
    <%--left: 'right',--%>
    <%--top: 'center',--%>
    <%--feature: {--%>
    <%--mark: {show: true},--%>
    <%--dataView: {show: true, readOnly: false},--%>
    <%--restore: {show: true},--%>
    <%--saveAsImage: {show: true}--%>
    <%--}--%>
    <%--},--%>
    <%--series: [--%>
    <%--{--%>
    <%--name: '用户人数',--%>
    <%--type: 'map',--%>
    <%--mapType: 'china',--%>
    <%--roam: false,--%>
    <%--label: {--%>
    <%--normal: {--%>
    <%--show: false--%>
    <%--},--%>
    <%--emphasis: {--%>
    <%--show: true--%>
    <%--}--%>
    <%--},--%>
    <%--data: data--%>
    <%--}--%>
    <%--]--%>
    <%--};--%>

    <%--// 使用刚指定的配置项和数据显示图表。 将配置赋值给图表对象--%>
    <%--china.setOption(option);--%>
    //     }
    // });
    // 多线程Ajax的形式
    $.ajax({
        url: '${pageContext.request.contextPath}/user/selectAllCountThread'
        , success: function (data) {
            console.info(data);
            console.info(data.count.one);
            // console.info(data.provinceMap)
            var man = data.sexMap[1].value;
            // var man = data.sexMap[1].value;
            console.info(man + "hhh")
            var women = data.sexMap[0].value;
            // 指定图表的配置项和数据
            var option1 = {
                //图表的标题
                title: {
                    text: '持明法洲男女人数对比'
                },
                tooltip: {},
                //legend 传奇 数学中 系列【图表】 在图表中展示的数据类型
                legend: {
                    data: ['人数']
                },
                // x轴坐标
                xAxis: {
                    data: ["男", "女"]
                },
                // y轴自动识别不需要写
                yAxis: {},
                // 数据  给x轴赋值
                series: [{
                    // name 必须 和 legend中类型一样
                    name: '人数',
                    // type 定义图片的展示样式 bar柱状图 line折线图 per饼状图
                    type: 'bar',
                    // data x轴坐标对应的坐标值 必须和x轴坐标一一对应
                    data: [man, women]
                }]
            };
            // 使用刚指定的配置项和数据显示图表。 将配置赋值给图表对象
            myChart.setOption(option1);

            // 注册量变化的图表赋值
                    var option2 = {
                        //图表的标题
                        title: {
                            text: '持明法洲用户注册量变化'
                        },
                        tooltip: {},
                        //legend 传奇 数学中 系列【图表】 在图表中展示的数据类型
                        legend: {
                            data: ['注册量']
                        },
                        // x轴坐标
                        xAxis: {
                            data: ["一周", "两周", "三周"]
                        },
                        // y轴自动识别不需要写
                        yAxis: {},
                        // 数据  给x轴赋值
                        series: [{
                            // name 必须 和 legend中类型一样
                            name: '人数',
                            // type 定义图片的展示样式 bar柱状图 line折线图 per饼状图
                            type: 'line',
                            // data x轴坐标对应的坐标值 必须和x轴坐标一一对应
                            data: [data.count.one, data.count.two, data.count.three]
                        }]
                    };

                    // 使用刚指定的配置项和数据显示图表。 将配置赋值给图表对象
                    myChart2.setOption(option2);

                    // 中国的图表赋值

                    var option3 = {
                        title: {
                            text: '用户地区分布',
                            left: 'center'
                        },
                        tooltip: {
                            trigger: 'item'
                        },
                        legend: {
                            orient: 'vertical',
                            left: 'left',
                            data: ['用户人数']
                        },
                        visualMap: {
                            min: 0,
                            max: 2500,
                            left: 'left',
                            top: 'bottom',
                            text: ['高', '低'],           // 文本，默认为数值文本
                            calculable: true
                        },
                        toolbox: {
                            show: true,
                            orient: 'vertical',
                            left: 'right',
                            top: 'center',
                            feature: {
                                mark: {show: true},
                                dataView: {show: true, readOnly: false},
                                restore: {show: true},
                                saveAsImage: {show: true}
                            }
                        },
                        series: [
                            {
                                name: '用户人数',
                                type: 'map',
                                mapType: 'china',
                                roam: false,
                                label: {
                                    normal: {
                                        show: false
                                    },
                                    emphasis: {
                                        show: true
                                    }
                                },
                                data: data.provinceMap
                            }
                        ]
                    };

                    // 使用刚指定的配置项和数据显示图表。 将配置赋值给图表对象
                    china.setOption(option3);

                }
            });


</script>
</body>
</html>