<%@page isELIgnored="false" pageEncoding="UTF-8" contentType="text/html; UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <title>用户管理</title>
    <meta name="viewport" content="width=device-width,initial-scale=1" charset="utf-8">
    <script src="${pageContext.request.contextPath}/layui/layui.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css" type="text/css">
    <script type="text/javascript">
        //加载要用到的模块
        layui.use(['table','jquery','layer','form', 'form','upload','element'],function () {
            var table = layui.table;
            var $ = layui.jquery;
            var layer = layui.layer;
            var form = layui.form;
            var upload = layui.upload;
            var element = layui.element;
        //表格的渲染
        table.render({
            elem:"#myTable"
            ,url:'${pageContext.request.contextPath}/log/selectAllLog'
            ,page:true
            ,toolbar:'#myToolBar'
            ,cols:[[
                {type:'checkbox' ,rowspan:2}
                ,{type:'numbers' ,rowspan:2}
                ,{field:'logId' ,title:'日志ID',rowspan:2}
                ,{field:'logAdminName' ,title:'所属用户',rowspan:2}
                ,{field:'logDate' ,title:'访问时间',rowspan:2,templet:"myLogDateTemplet"}
                ,{field:'logIp' ,title:'所属IP',rowspan:2}
                ,{field:'logType' ,title:'请求类型',rowspan:2}
                ,{field:'logContent' ,title:'日志内容',rowspan:2}

                ]]
        });
        //监听工具栏事件
            var layerIndex=0;
            table.on("toolbar(myTableFilter)",function (obj) {
               console.info(obj)
                switch (obj.event) {
                    case 'multiDelete':
                        //批量删除
                        //1、获取到选中的数据
                        var selectRows = table.checkStatus("myTable");
                        console.info("批量删除"+selectRows);
                        if(selectRows.data.length==0){
                            //给提示
                            layer.msg("请选中要删除的行",{icon:4})
                        }else{
                            layer.confirm("真的删除选中的所有行，不可恢复？",{
                               title:'删除确认框',icon:3},function (index) {
                                //点击确认的时候会进到这个函数
                                //关闭此确认框
                                layer.close(index)
                                var logIds = new Array();
                                $.each(selectRows.data,function (index,obj) {
                                   logIds[index]=obj.logId;
                                });
                                //发送Ajax请求
                                $.ajax({

                                });
                            });
                        }
                        break;
                    case 'search':
                        //1、获取到页面输入框输入的内容
                        //2、发送Ajax请求到后台，执行查询操作，然后刷新(reload)table
                        var name = $("#searchName").val()
                        console.info("关键字"+name);
                        //3、刷新表单 重新加载
                        table.reload("myTable",{"where":{"name":name}})
                }
            });
    });
    </script>

</head>
<script type="text/html" id="myToolBar">
    <div class="layui-btn-container layui-inline">
        <button class="layui-btn layui-btn-normal" lay-event="multiDelete">批量删除</button>
    </div>
    <%--搜索功能对应的页面效果--%>
    <div class="layui-inline">
        <div class="layui-inline">
        <label class="layui-inline">用户名</label>
            <div class="layui-input-inline" style="width: 200px">
                <input type="text" id="searchName" name="name" placeholder="关键字" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px">
                <input type="button" class="layui-btn layui-btn-normal" lay-event="search" value="搜索">
            </div>
        </div>
    </div>
</script>
<script type="text/html" id="myLogDateTemplet">
    {{layui.util.toDateString(d.LogDate, 'yyyy-MM-dd')}}
</script>
<body>
    <table id="myTable" lay-filter="myTableFilter"></table>
</body>