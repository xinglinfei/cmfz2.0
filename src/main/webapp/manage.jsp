<%@page isELIgnored="false" pageEncoding="UTF-8" contentType="text/html; UTF-8" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>layout 后台大布局 - Layui</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css">
</head>
<script src="${pageContext.request.contextPath}/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>

<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">layui 后台布局</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="">控制台</a></li>
            <li class="layui-nav-item"><a href="">商品管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    贤心
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="">退了</a></li>
        </ul>
    </div>


    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" id="nav" lay-filter="test1">


            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">

            <div class="layui-tab layui-tab-brief layui-tab-card" lay-allowClose="true" lay-filter="myTab">
                <ul class="layui-tab-title">
                    <li class="layui-this">欢迎页</li>
                </ul>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">欢迎使用..xxxx</div>
                </div>
            </div>

        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>


</div>

<script>

    $(function () {
        // 1.获取到后台的数据
        $.ajax({
            url: "${pageContext.request.contextPath}/menu/selectAll",
            success: function (data) {
                // 处理响应的数据  data 是一个数组 集合
                // 2.遍历数据 展示一级菜单 拼接出来一级菜单的html 写到nav的位置
                var content = "";
                // each遍历 参数1 被遍历的数据  参数2 匿名函数  匿名函数参数1位置 下标  参数2位置 被遍历到的元素
                $.each(data, function (index1, menu1) {
                    // 拼接一级菜单的开始标签  \"  这个\叫转义符号 在js中双引号 单引号都是语法含义的 如果不希望双引号被js解析  需要添加转义
                    content += "<li class=\"layui-nav-item layui-nav-itemed\"><a class=\"\" href=\"javascript:;\">";
                    // 拼接一级菜单的名字
                    content += menu1.menuName + "</a><dl class=\"layui-nav-child\">";
                    // 3.拼接二级菜单内容
                    $.each(menu1.child, function (index2, menu2) {
                        // 拼接二级菜单的开始标签
                        content += "<dd><a href=\"javascript:;\" onclick='addTabs(\"" + menu2.menuName + "\",\"" + menu2.menuUrl + "\"," + menu2.menuId + ")'>";
                        // 拼接二级菜单的名字
                        content += menu2.menuName;
                        // 拼接二级菜单的结束标签
                        content += "</a></dd>";
                    })

                    // 拼接一级菜单的结束标签
                    content += "</dl></li>";
                })

                $("#nav").html(content);

                //更新渲染
                layui.use('element', function () {
                    var element = layui.element;
                    element.render('nav', 'test1');
                });

            }
        })
    });

    function addTabs(menuName, menuUrl, menuId) {
        layui.use(['element','jquery'], function () {
            var element = layui.element;
            var $ = layui.jquery;
            var exist = $("li[lay-id="+menuId+"]").length;
            console.info(exist);
            // 添加选项卡
            if (exist == 0) {
                element.tabAdd('myTab', {
                    //
                    title: menuName,
                    // 二级菜单页面 需要得到二级菜单的menuUrl属性值
                    content: '<iframe src="${pageContext.request.contextPath}' + menuUrl + '" height="100%" width="100%"></iframe>',
                    // 二级菜单的id
                    id: menuId
                });
            }
            // 切换到新添加的选项卡
            element.tabChange('myTab', menuId);
        });
    }
</script>
</body>
</html>